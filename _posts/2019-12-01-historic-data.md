---
path: '/sputnik'
title: 'historic data'
category: sputnik
layout: nil
---

This file is stored in `src/historic_data.ts`

The whole goal of this functions are to download historic data and verify the continuity of the database time-series

Using the values in the `config.json` file, this file will:
- loop through every configured exchanges every 20 minutes
  
  ``` 
  "exchanges": [
		{
			"name": "binance",
			"key": "API_KEY",
			"secret": "API_SECRET",
			"whitelist": ["BTC"],
			"blacklist": [],
            "history": 8650,
            "gap": 1
		}
	]
    ```
- For every exchange, loop through all the allowed markets (white/black lists) 
- For every market, save the market candle from the last (exchange.gap), by default it's 1m and the oldest specified data (exchange.history)
- Then, efficiently verify the continuity of the time-serie data for this market by isolating all the gaps (exchange.gap) missing.
- Starting by the oldest gap, request the data from the exchange. On binance, this returns at most 500 candles, so we can skip the next 499 gaps.

All candle data are stores in the OHLCV format.