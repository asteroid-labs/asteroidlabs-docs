---
path: '/sputnik'
title: 'latest price'
category: sputnik
layout: nil
---

This file is stored in `src/latest_price.ts`

This file will subscribe using websocket to every price updates (trades).

Following the format of `candles`, it will loop through all markets and subscribe to their websocket feed (if supported by the exchange)

This way, we can save all the future markets prices, in almost real-time, whitout querying the exchange(s).

All candle data are stores in the OHLCV format, as the latest entry in the database and get overwritten on change.