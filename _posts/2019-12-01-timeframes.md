---
path: '/vostok'
title: 'timeframes'
category: vostok
layout: nil
---

If you are the administrator, you'll be able to add and edit timeframes for your vostok instance.
timeframes have a `name` attribute and a `msec_value` (for example, 6000 for 1m).