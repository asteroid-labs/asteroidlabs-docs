---
path: '/sputnik'
title: 'candles'
category: sputnik
layout: nil
---

This file is stored in `src/candles.ts`

This file will subscribe using websocket to every candle updates on the 1m timeframe (configurable in config.exchange)

Following the format of `historic_data`, it will loop through all markets and subscribe to their websocket feed (if supported by the exchange)

This way, we can save all the future markets changes, whitout querying the exchange(s).

All candle data are stores in the OHLCV format.