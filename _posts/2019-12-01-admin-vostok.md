---
path: '/vostok'
title: 'Admin'
category: vostok
layout: nil
---

The admin page enables you to read and edit your config.json settings while the server is running.