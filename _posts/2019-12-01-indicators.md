---
path: '/vostok'
title: 'indicators'
category: vostok
layout: nil
---

If you are the administrator, you'll be able to add and edit indicators for your vostok instance.
For each indicator, you will need a corresponding file in `backtest/custom/` to calculate something on every candle data. Don't forget to import it in the `backtest/custom/index.ts`

indicators have a `name` attribute, a `function name` (if in the tulind library, it's fnc name, or the name of your custom import). If you need to calculate value using multiple timeframes, check the 'Is Composite?' button, and edit the values at your liking